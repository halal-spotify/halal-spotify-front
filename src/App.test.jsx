import App from "./App";
import { render, screen } from "./utils/test-utils";

describe("First load test", () => {
  it("Check if the text 'Hello world!' really shows up", () => {
    render(<App />);
    const found = screen.getByText(/Hello world!/i);
    expect(found).toBeInTheDocument();
  });
});
