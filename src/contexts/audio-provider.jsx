import { useState, useEffect, createContext, useRef } from "react";
import PropTypes from "prop-types";

export const AudioContext = createContext();

export const AudioProvider = ({ children }) => {
  const audioRef = useRef(new Audio());
  const [playList, setPlayList] = useState(
    localStorage.getItem("playlist")
      ? JSON.parse(localStorage.getItem("playlist"))
      : null,
  );
  const [track, setTrack] = useState(
    localStorage.getItem("track")
      ? JSON.parse(localStorage.getItem("track"))
      : null,
  );
  const [tracks, setTracks] = useState(
    localStorage.getItem("playlist")
      ? JSON.parse(localStorage.getItem("playlist"))?.medias
      : [],
  );
  const [volume, setVolume] = useState(
    localStorage.getItem("volume") ? localStorage.getItem("volume") : 20,
  );
  const [isPlaying, setIsPlaying] = useState(false);

  useEffect(() => {
    if (!track) return;
    localStorage.setItem("track", JSON.stringify(track));
    audioRef.current.pause();
  }, [track]);

  useEffect(() => {
    audioRef.current.volume = volume / 100;
    console.log("volume changed", audioRef.current.volume);
    localStorage.setItem("volume", volume);
  }, [volume]);

  useEffect(() => {
    if (!playList) return;
    localStorage.setItem("playlist", JSON.stringify(playList));
    setTracks(playList?.medias);
  }, [playList]);

  return (
    <AudioContext.Provider
      value={{
        audioRef,
        track,
        setTrack,
        tracks,
        setTracks,
        volume,
        setVolume,
        playList,
        setPlayList,
        isPlaying,
        setIsPlaying,
      }}
    >
      {children}
    </AudioContext.Provider>
  );
};

AudioProvider.propTypes = {
  children: PropTypes.node,
};
