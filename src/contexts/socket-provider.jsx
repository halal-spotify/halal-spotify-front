// SocketContext.js
import { createContext, useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import io from "socket.io-client";

export const SocketContext = createContext();

export const SocketProvider = ({ children }) => {
  const socketRef = useRef();
  const [shareListenning, setShareListenning] = useState(false);

  useEffect(() => {
    // Connect to the WebSocket server
    socketRef.current = io("http://vps-eb999426.vps.ovh.net:5001");
    console.log("socketRef", socketRef);
    // Clean up the socket connection when the component unmounts
    return () => {
      socketRef.current.disconnect();
    };
  }, []);

  return (
    <SocketContext.Provider
      value={{
        socketRef,
        shareListenning,
        setShareListenning,
      }}
    >
      {children}
    </SocketContext.Provider>
  );
};
SocketProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
