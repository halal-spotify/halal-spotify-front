import axios from "axios";
import { RouterProvider, createBrowserRouter } from "react-router-dom";

import {
  RootScreen,
  HomeScreen,
  TracksScreen,
  AlbumsScreen,
  ArtistsScreen,
  LoginScreen,
  RegisterScreen,
  NotFoundScreen,
  UserScreen,
  UserAlbumScreen,
} from "@/screens";
import { TooltipProvider } from "@/components/ui";

import { ThemeProvider } from "@/contexts/theme-provider";
import { AudioProvider } from "@/contexts/audio-provider";
import { SocketProvider } from "@/contexts/socket-provider";

axios.defaults.baseURL = "vps-eb999426.vps.ovh.net:5001";
axios.defaults.withCredentials = true;
// axios.defaults.baseURL = "http://localhost:3000";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootScreen />,
    errorElement: <NotFoundScreen />,
    children: [
      {
        path: "/",
        element: <HomeScreen />,
      },
      {
        path: "tracks",
        element: <TracksScreen />,
      },
      {
        path: "albums",
        Component: AlbumsScreen,
      },
      {
        path: "artists",
        Component: ArtistsScreen,
      },
      {
        path: "user/:username",
        Component: UserScreen,
      },
      {
        path: "album/:albumID",
        Component: UserAlbumScreen,
      },
    ],
  },
  {
    path: "/login",
    Component: LoginScreen,
  },
  {
    path: "/register",
    Component: RegisterScreen,
  },
  {
    path: "*",
    Component: NotFoundScreen,
  },
]);

function App() {
  return (
    <ThemeProvider defaultTheme="light" storageKey="vite-ui-theme">
      <TooltipProvider>
        <AudioProvider>
          <SocketProvider>
            <RouterProvider router={router} />
          </SocketProvider>
        </AudioProvider>
      </TooltipProvider>
    </ThemeProvider>
  );
}

export default App;
