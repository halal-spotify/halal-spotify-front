import PropTypes from "prop-types";
// import { PlusCircledIcon } from "@radix-ui/react-icons";
import { cn } from "@/utils";
import {
  ContextMenu,
  ContextMenuContent,
  ContextMenuItem,
  ContextMenuSeparator,
  // ContextMenuSub,
  // ContextMenuSubContent,
  // ContextMenuSubTrigger,
  ContextMenuTrigger,
} from "@/components/ui/context-menu";

import axios from "axios";

export function AlbumArtwork({
  album,
  aspectRatio = "portrait",
  width,
  height,
  playClick,
  className,
  ...props
}) {
  return (
    <div className={cn("space-y-3 hover:cursor-pointer", className)} {...props}>
      <ContextMenu>
        <ContextMenuTrigger>
          <div className="overflow-hidden rounded-md group relative">
            <img
              src={axios.defaults.baseURL + "/albums/cover/" + album.id}
              alt={album.name}
              width={width}
              height={height}
              className={cn(
                "h-auto w-auto object-cover transition-all hover:scale-105",
                aspectRatio === "portrait" ? "aspect-[3/4]" : "aspect-square",
              )}
            />
            <div
              onClick={(e) => {
                e.stopPropagation();
                e.preventDefault();
                playClick();
              }}
              className="invisible group-hover:visible bg-slate-900 absolute bottom-0 right-0 pl-2.5 pr-2 py-2.5 rounded-ss-md hover:text-[#13d339]"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <polygon points="5 3 19 12 5 21 5 3" />
              </svg>
            </div>
          </div>
        </ContextMenuTrigger>
        <ContextMenuContent className="w-40">
          <ContextMenuItem>Add to Library</ContextMenuItem>
          {/* <ContextMenuSub>
            <ContextMenuSubTrigger>Add to Playlist</ContextMenuSubTrigger>
            <ContextMenuSubContent className="w-48">
              <ContextMenuItem>
                <PlusCircledIcon className="mr-2 h-4 w-4" />
                New Playlist
              </ContextMenuItem>
              <ContextMenuSeparator />
              {playlists.map((playlist) => (
                <ContextMenuItem key={playlist}>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    className="mr-2 h-4 w-4"
                    viewBox="0 0 24 24"
                  >
                    <path d="M21 15V6M18.5 18a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5ZM12 12H3M16 6H3M12 18H3" />
                  </svg>
                  {playlist}
                </ContextMenuItem>
              ))}
            </ContextMenuSubContent>
          </ContextMenuSub> */}
          <ContextMenuSeparator />
          <ContextMenuItem>Play Next</ContextMenuItem>
          <ContextMenuItem>Play Later</ContextMenuItem>
          <ContextMenuItem>Create Station</ContextMenuItem>
          <ContextMenuSeparator />
          <ContextMenuItem>Like</ContextMenuItem>
          <ContextMenuItem>Share</ContextMenuItem>
        </ContextMenuContent>
      </ContextMenu>
      <div className="space-y-1 text-sm">
        <h3 className="font-medium leading-none">{album.name}</h3>
        <p className="text-xs text-muted-foreground">
          {album.users.map((user) => user.name).join(", ")}
        </p>
      </div>
    </div>
  );
}

AlbumArtwork.propTypes = {
  album: PropTypes.shape({
    name: PropTypes.string,
    id: PropTypes.number,
    users: PropTypes.arrayOf(PropTypes.object),
  }),
  aspectRatio: PropTypes.oneOf(["portrait", "square"]),
  width: PropTypes.number,
  height: PropTypes.number,
  playClick: PropTypes.func,
  className: PropTypes.string,
};
