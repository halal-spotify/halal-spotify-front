import { useContext, useEffect, useRef, useState } from "react";
import { PauseIcon, Volume2, VolumeX } from "lucide-react";
import { Button, Slider } from "@/components/ui";
import { AudioContext } from "@/contexts/audio-provider";
import { SocketContext } from "@/contexts/socket-provider";
import axios from "axios";

export function AudioPlayer() {
  const { socketRef, shareListenning } = useContext(SocketContext);
  const {
    track,
    setTrack,
    tracks,
    volume,
    setVolume,
    setTracks,
    audioRef,
    isPlaying,
    setIsPlaying,
    playList,
    setPlayList,
  } = useContext(AudioContext);

  const [isShuffling, setIsShuffling] = useState(false);
  const [isLooping, setIsLooping] = useState(false);
  const [currentAudioTime, setCurrentAudioTime] = useState(0);
  const [selectedTrackCFurl, setSelectedTrackCFurl] = useState("");
  const [isMuted, setIsMuted] = useState(false);

  useEffect(() => {
    if (!shareListenning) return;
    console.log("EMITING");
    if (socketRef?.current && track) {
      socketRef.current.emit("changePLaylist", { playList });
      socketRef.current.emit("changeTrack", { track });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [socketRef?.current, track?.id]);

  useEffect(() => {
    if (!shareListenning) return;
    console.log("LISTENNING");

    // Listen for playlist changes
    if (!socketRef?.current) return;
    socketRef?.current.on("changePLaylist", (data) => {
      console.log("Received changePLaylist:", data);

      setPlayList(data.playlist);
    });
    // Listen for track changes
    socketRef?.current.on("changeTrack", (data) => {
      console.log("Received changeTrack:", data);

      setTrack(data.track);
    });
    // Listen for play/stop changes
    socketRef?.current.on("changeIsPlaying", (data) => {
      console.log("Received changeIsPlaying:", data);

      setIsPlaying(data.isPlaying);
      data.isPlaying ? audioRef.current?.play() : audioRef.current?.pause();
    });
    // Listen for play/start changes
    socketRef?.current.on("changeTime", (data) => {
      console.log("Received changeTime:", data);

      setCurrentAudioTime(data.newTime);
      audioRef.current.currentTime = data.newTime;
    });

    return () => {
      // Clean up event listener on component unmount
      // eslint-disable-next-line react-hooks/exhaustive-deps
      socketRef?.current.off("changeTrack");
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [socketRef.current, shareListenning]);

  useEffect(() => {
    if (!shareListenning) return;
    console.log("EMITING");
    if (socketRef?.current && track) {
      socketRef?.current.emit("changeIsPlaying", { isPlaying });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isPlaying]);

  //handle track change
  useEffect(() => {
    const audio = audioRef.current;

    if (!track?.id) return;

    // if (isPlaying) {
    //   setIsPlaying(false);
    //   audio.pause();
    // }
    setSelectedTrackCFurl(axios.defaults.baseURL + "/media/stream/" + track.id);
    audio.src = axios.defaults.baseURL + "/media/stream/" + track.id;

    if (isPlaying) {
      setIsPlaying(true);
      audio.play();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [track?.id, playList]);

  useEffect(() => {
    if (!tracks?.length > 0) return;
    if (isShuffling) randomTrack();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tracks]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handlePlayPause = () => {
    setIsPlaying(!isPlaying);
  };

  //handle next track
  const handleNext = () => {
    const currentIndex = tracks.findIndex(
      (track) =>
        axios.defaults.baseURL + "/media/stream/" + track.id ===
        selectedTrackCFurl,
    );
    const nextIndex = (currentIndex + 1) % tracks.length;
    setSelectedTrackCFurl(
      axios.defaults.baseURL + "/media/stream/" + tracks[nextIndex]?.id,
    );
    setTrack(tracks[nextIndex]);

    audioRef.current.load();
    setIsPlaying(false);
  };

  //handlePrevious Track
  const handlePrevious = () => {
    //replay track at start if already started
    if (audioRef.current.currentTime >= 4) {
      audioRef.current.currentTime = 0;
      return;
    }
    const currentIndex = tracks.findIndex(
      (track) =>
        axios.defaults.baseURL + "/media/stream/" + track.id ===
        selectedTrackCFurl,
    );
    // Calculate the previous index, considering the possibility of negative values
    const previousIndex = (currentIndex - 1 + tracks.length) % tracks.length;
    setSelectedTrackCFurl(
      axios.defaults.baseURL + "/media/stream/" + tracks[previousIndex]?.id,
    );
    setTrack(tracks[previousIndex]);

    audioRef.current.load();
    setIsPlaying(false);
  };

  //handle audio time update
  useEffect(() => {
    const handleTimeUpdate = () => {
      setCurrentAudioTime(audioRef.current.currentTime);
    };
    const intervalId = setInterval(() => {
      handleTimeUpdate();
    }, 1000);
    return () => {
      clearInterval(intervalId);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  //handle start/pause with spacebar
  useEffect(() => {
    const handleKeyUp = (e) => {
      //toggle the play/pause if the spacebar is pressed and the target is not an input
      if (e.key === " " && e.target.tagName !== "INPUT") {
        handlePlayPause();
      }
    };

    document.addEventListener("keyup", handleKeyUp);

    return () => {
      // Cleanup the event listener when the component unmounts
      document.removeEventListener("keyup", handleKeyUp);
    };
  }, [handlePlayPause]);

  //handle audio play/pause and end of track
  useEffect(() => {
    const audio = audioRef.current;
    if (isPlaying) {
      audioRef.current.play();
      document.title = track?.name;
    } else {
      audioRef.current.pause();
    }

    if (isPlaying && audioRef.current.currentTime >= audio.duration - 1) {
      console.log("end of track");
      // setCurrentAudioTime(0);
      if (isLooping) {
        console.log("looping");
        console.log("currentAudioTime", currentAudioTime);
        handlePrevious();
      } else {
        console.log("not looping");
        handleNext();
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isPlaying, audioRef.current.currentTime]);

  //display time in minutes and seconds
  const formatTime = (timeInSeconds) => {
    if (!timeInSeconds) {
      return "0:00";
    }

    const minutes = Math.floor(timeInSeconds / 60);
    const seconds = Math.floor(timeInSeconds % 60);

    return `${minutes}:${String(seconds).padStart(2, "0")}`;
  };

  //handle shuffle
  const randomTrack = () => {
    setTracks(tracks.sort(() => Math.random() - 0.5));
  };

  //handle change time
  const handleChangeTime = (value) => {
    if (Number.isNaN(value)) return;
    setCurrentAudioTime(value[0]);
    audioRef.current.currentTime = value;
    console.log(audioRef.current.currentTime);
  };

  //handle shuffle
  useEffect(() => {
    if (isShuffling) {
      randomTrack();
    } else {
      if (tracks) setTracks(tracks.sort((a, b) => a.id - b.id));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isShuffling]);

  const handleSetVolume = (value) => {
    if (Number.isNaN(value)) return;
    if (value === 0) setIsMuted(true);
    setVolume(value);
    if (isMuted) {
      setIsMuted(false);
    }
  };

  const handleMute = () => {
    setIsMuted((prev) => !prev);
  };

  const isInitialRender = useRef(true);
  const [lastVolume, setLastVolume] = useState(20);

  // Update volume if muted
  useEffect(() => {
    if (isInitialRender.current) {
      isInitialRender.current = false;
      return; // Skip the effect on initial render
    }

    if (isMuted) {
      setLastVolume(volume);
      setVolume(0);
    } else {
      setVolume(lastVolume);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isMuted]);

  if (!track)
    return (
      <div className="fixed bottom-0 w-full">
        <div className="flex items-center center justify-center h-36 bg-background dark:bg-background border-t shadow-md overflow-hidden">
          No track selected
        </div>
      </div>
    );

  return (
    <div className="fixed bottom-0 w-full">
      <div className="flex flex-row items-center justify-between bg-background dark:bg-background border-t shadow-md overflow-hidden">
        <div className="flex flex-row items-center">
          <div className="flex items-center justify-center h-36">
            <img
              alt="Album cover"
              className="object-cover w-full h-full"
              height={200}
              src={axios.defaults.baseURL + "/media/cover/" + track?.id}
              style={{
                aspectRatio: "200/200",
                objectFit: "cover",
              }}
              width={200}
            />
          </div>
          <div className="px-6 py-4">
            <h1 className="text-xl font-bold">{track.name}</h1>
            <p className="text-sm text-gray-500 dark:text-gray-400">
              {track.users.map((user) => user.name).join(", ")}
            </p>
            {Object.values(playList).length > 0 && (
              <p className="text-sm text-gray-500 dark:text-gray-400">
                {playList.name}
              </p>
            )}
          </div>
        </div>
        <div>
          <div className="px-6 py-2 flex justify-center items-center gap-3">
            <Button
              size="icon"
              variant="outline"
              onClick={() => setIsShuffling(!isShuffling)}
              disabled={Object.values(playList).length === 0}
            >
              {isShuffling ? (
                <ShuffleIcon className="h-5 w-5 text-[#13d339]" />
              ) : (
                <ShuffleIcon className="h-5 w-5" />
              )}
              <span className="sr-only">Shuffle</span>
            </Button>
            <Button
              size="icon"
              variant="outline"
              onClick={handlePrevious}
              disabled={Object.values(playList).length === 0}
            >
              <ArrowLeftIcon className="h-5 w-5" />
              <span className="sr-only">Previous</span>
            </Button>
            <Button size="icon" onClick={handlePlayPause}>
              {isPlaying ? (
                <>
                  <PauseIcon className="h-5 w-5" />
                  <span className="sr-only">Pause</span>
                </>
              ) : (
                <>
                  <PlayIcon className="h-5 w-5" />
                  <span className="sr-only">Play</span>
                </>
              )}
            </Button>
            <Button
              size="icon"
              variant="outline"
              onClick={handleNext}
              disabled={Object.values(playList).length === 0}
            >
              <ArrowRightIcon className="h-5 w-5" />
              <span className="sr-only">Next</span>
            </Button>
            <Button
              size="icon"
              variant="outline"
              onClick={() => setIsLooping((prev) => !prev)}
            >
              {isLooping ? (
                <RepeatIcon className="h-5 w-5 text-[#13d339]" />
              ) : (
                <RepeatIcon className="h-5 w-5" />
              )}
              <span className="sr-only">Repeat</span>
            </Button>
          </div>
          <div className="px-6 py-2 w-[500px] flex gap-4">
            <span className="text-xs">{formatTime(currentAudioTime)}</span>
            <Slider
              type="range"
              aria-label="Playback progress"
              className="w-full"
              value={[currentAudioTime]}
              min={0}
              max={audioRef.current.duration}
              onValueChange={handleChangeTime}
              step={1}
            />
            <span className="text-xs">
              {formatTime(audioRef.current.duration || track?.duration)}
            </span>
          </div>
        </div>
        <div className="w-40">
          <div className="px-6 py-2 flex justify-center items-center gap-3">
            <Button
              size="icon"
              variant="outline"
              className="px-2"
              onClick={handleMute}
            >
              {isMuted ? (
                <VolumeX className="h-5 w-5" />
              ) : (
                <Volume2 className="h-5 w-5" />
              )}
              <span className="sr-only">Volume</span>
            </Button>
            <Slider
              aria-label="Volume"
              className="w-full"
              value={[volume]}
              onValueChange={handleSetVolume}
              step={1}
              min={0}
              max={100}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

function ArrowLeftIcon(props) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="m12 19-7-7 7-7" />
      <path d="M19 12H5" />
    </svg>
  );
}

function ArrowRightIcon(props) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M5 12h14" />
      <path d="m12 5 7 7-7 7" />
    </svg>
  );
}

function PlayIcon(props) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <polygon points="5 3 19 12 5 21 5 3" />
    </svg>
  );
}

function RepeatIcon(props) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="m17 2 4 4-4 4" />
      <path d="M3 11v-1a4 4 0 0 1 4-4h14" />
      <path d="m7 22-4-4 4-4" />
      <path d="M21 13v1a4 4 0 0 1-4 4H3" />
    </svg>
  );
}

function ShuffleIcon(props) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M2 18h1.4c1.3 0 2.5-.6 3.3-1.7l6.1-8.6c.7-1.1 2-1.7 3.3-1.7H22" />
      <path d="m18 2 4 4-4 4" />
      <path d="M2 6h1.9c1.5 0 2.9.9 3.6 2.2" />
      <path d="M22 18h-5.9c-1.3 0-2.6-.7-3.3-1.8l-.5-.8" />
      <path d="m18 14 4 4-4 4" />
    </svg>
  );
}
