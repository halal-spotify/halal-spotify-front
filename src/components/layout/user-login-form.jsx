import PropTypes from "prop-types";
import { useState } from "react";

// import { useSignIn } from "react-auth-kit";
// import { useNavigate } from "react-router-dom";

import { cn } from "@/utils";
import { Icons, Button, Input, Label } from "@/components/ui";

export function UserLoginForm({ className, ...props }) {
  // const signIn = useSignIn();
  // const navigate = useNavigate();

  const [isLoading, setIsLoading] = useState(false);

  async function onSubmit(event) {
    event.preventDefault();
    setIsLoading(true);

    // fetch login api
    // if (
    //   signIn({
    //     token: "35v3443bn368367n306306wbn407qn420b436b4", //Just a random token
    //     tokenType: "Bearer", // Token type set as Bearer
    //     authState: { name: "React User", uid: 123456 }, // Dummy auth user state
    //     expiresIn: 10, // Token Expriration time, in minutes
    //     refreshToken: "23mv86n790g4vm2706c2m38v6n790",
    //     refreshTokenExpireIn: 60,
    //   })
    // ) {
    //   // If Login Successfull, then Redirect the user to secure route
    //   navigate("/secure");
    // } else {
    //   // Else, there must be some error. So, throw an error
    //   alert("Error Occoured. Try Again");
    // }

    setTimeout(() => {
      setIsLoading(false);
    }, 3000);
  }

  return (
    <div className={cn("grid gap-6", className)} {...props}>
      <form onSubmit={onSubmit}>
        <div className="grid gap-2">
          <div className="grid gap-1">
            <Label htmlFor="email">Email</Label>
            <Input
              id="email"
              placeholder="name@example.com"
              type="email"
              autoCapitalize="none"
              autoComplete="email"
              autoCorrect="off"
              disabled={isLoading}
            />
          </div>
          <div className="grid gap-1">
            <Label htmlFor="password">Password</Label>
            <Input
              id="password"
              placeholder="***"
              type="password"
              autoCapitalize="none"
              autoComplete="password"
              autoCorrect="off"
              disabled={isLoading}
            />
          </div>
          <Button className="mt-3" disabled={isLoading}>
            {isLoading && (
              <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />
            )}
            Sign In with Email
          </Button>
        </div>
      </form>
    </div>
  );
}

UserLoginForm.propTypes = {
  className: PropTypes.string,
};
