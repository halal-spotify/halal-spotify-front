import { AuthLayout, UserLoginForm } from "@/components/layout";
// import { useIsAuthenticated } from "react-auth-kit";
// import { Navigate } from "react-router-dom";

export function LoginScreen() {
  // const isAuthenticated = useIsAuthenticated();

  // if (isAuthenticated()) {
  //   return <Navigate to={"/"} replace />;
  // }

  return (
    <AuthLayout page={{ link: "/auth/register", title: "Sign in" }}>
      <div className="lg:p-8">
        <div className="mx-auto flex w-full flex-col justify-center space-y-6 sm:w-[350px]">
          <div className="flex flex-col space-y-2 text-center">
            <h1 className="text-2xl font-semibold tracking-tight">Login</h1>
            <p className="text-sm text-muted-foreground">
              Enter your email & password below to login to your account
            </p>
          </div>
          <UserLoginForm />
          <p className="px-8 text-center text-sm text-muted-foreground">
            By logging in, you agree to our{" "}
            <a
              href="/terms"
              className="underline underline-offset-4 hover:text-primary"
            >
              Terms of Service
            </a>{" "}
            and{" "}
            <a
              href="/privacy"
              className="underline underline-offset-4 hover:text-primary"
            >
              Privacy Policy
            </a>
            .
          </p>
        </div>
      </div>
    </AuthLayout>
  );
}
