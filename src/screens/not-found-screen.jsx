import { Ghost } from "lucide-react";
import { Link } from "react-router-dom";

export function NotFoundScreen() {
  return (
    <div className="h-screen text-center grid place-items-center">
      <div>
        <Ghost
          size={360}
          strokeWidth={1.25}
          className="text-muted-foreground"
        />
        <h1 className="mt-8 text-2xl font-semibold tracking-tight">
          Error, Page Not Found
        </h1>
        <p className="text-sm text-muted-foreground">
          This page doesn&apos;t exist, please check the URL and try again.{" "}
          <br />
          or <br />
          <Link
            href="/"
            className="underline underline-offset-4 hover:text-primary"
          >
            Go back to the homepage
          </Link>
        </p>
      </div>
    </div>
  );
}
