import { useContext, useEffect, useState } from "react";
import axios from "axios";

import {
  PageLayout,
  AlbumArtwork,
  MediaArtwork,
  UserArtwork,
} from "@/components/layout";
import { Separator, ScrollArea, ScrollBar, Icons } from "@/components/ui";
import { AudioContext } from "@/contexts/audio-provider";
import { useNavigate } from "react-router-dom";

export function HomeScreen() {
  const [mediaData, setMediaData] = useState(null);
  const [albumsData, setAlbumsData] = useState(null);
  const [usersData, setUsersData] = useState(null);

  const { setTrack, setTracks, setPlayList } = useContext(AudioContext);

  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get("/auth/tenusers")
      .then((response) => {
        setUsersData(response.data);
      })
      .catch((error) => {
        setUsersData([]);
        console.log(error);
      });
    axios
      .get("/albums/tenalbums")
      .then((response) => {
        setAlbumsData(response.data);
      })
      .catch((error) => {
        setAlbumsData([]);
        console.log(error);
      });
    axios
      .get("/media/tenmedia")
      .then((response) => {
        setMediaData(response.data);
      })
      .catch((error) => {
        setMediaData([]);
        console.log(error);
      });

    return () => {
      setUsersData(null);
      setMediaData(null);
      setAlbumsData(null);
    };
  }, []);

  if (usersData === null || mediaData === null || albumsData === null) {
    return (
      <div className="grid place-items-center">
        <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />
      </div>
    );
  }
  return (
    <PageLayout className="p-8">
      <div className="flex items-center justify-between">
        <div className="space-y-1">
          <h2 className="text-2xl font-semibold tracking-tight">
            10 Lastest Songs
          </h2>
          <p className="text-sm text-muted-foreground">
            Best 10 latest daily songs
          </p>
        </div>
      </div>
      <Separator className="my-4" />
      <div className="relative">
        <ScrollArea>
          <div className="flex space-x-4 pb-4">
            {mediaData.map((media) => (
              <MediaArtwork
                onClick={() => {
                  setTrack(media);
                  setPlayList({});
                }}
                key={media.id}
                media={media}
                className="w-[250px]"
                aspectRatio="portrait"
                width={250}
                height={250}
              />
            ))}
          </div>
          <ScrollBar orientation="horizontal" />
        </ScrollArea>
      </div>
      <div className="mt-6 space-y-1">
        <h2 className="text-2xl font-semibold tracking-tight">
          10 Latest Albums
        </h2>
        <p className="text-sm text-muted-foreground">
          Best 10 latest daily albums
        </p>
      </div>
      <Separator className="my-4" />
      <div className="relative">
        <ScrollArea>
          <div className="flex space-x-4 pb-4">
            {albumsData.map((album) => (
              <AlbumArtwork
                playClick={() => {
                  setTracks(album?.medias);
                  setPlayList(album);
                }}
                onClick={() => {
                  navigate(`/album/${album.id}`);
                }}
                key={album.id}
                album={album}
                className="w-[150px]"
                aspectRatio="square"
                width={150}
                height={150}
              />
            ))}
          </div>
          <ScrollBar orientation="horizontal" />
        </ScrollArea>
      </div>
      <div className="mt-6 space-y-1">
        <h2 className="text-2xl font-semibold tracking-tight">
          10 Latest Artists
        </h2>
        <p className="text-sm text-muted-foreground">
          Best 10 latest daily artists
        </p>
      </div>
      <Separator className="my-4" />
      <div className="relative">
        <ScrollArea>
          <div className="flex space-x-4 pb-4">
            {usersData.map((user) => (
              <UserArtwork
                onClick={() => {
                  navigate(`/user/${user.name}`);
                }}
                key={user.id}
                user={user}
                className="w-[50px]"
                aspectRatio="square"
                width={50}
                height={50}
              />
            ))}
          </div>
          <ScrollBar orientation="horizontal" />
        </ScrollArea>
      </div>
    </PageLayout>
  );
}
