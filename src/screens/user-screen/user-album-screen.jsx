import { useContext, useEffect, useState } from "react";
import axios from "axios";

import { PageLayout } from "@/components/layout";
import {
  ScrollArea,
  ScrollBar,
  Icons,
  Table,
  TableBody,
  TableCaption,
  TableCell,
  TableFooter,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui";
import { AudioContext } from "@/contexts/audio-provider";
import { useParams } from "react-router-dom";
import { Play } from "lucide-react";

export function UserAlbumScreen() {
  const [albumData, setAlbumData] = useState(null);

  const { setTrack, setTracks, setPlayList } = useContext(AudioContext);

  const { albumID } = useParams();

  useEffect(() => {
    axios
      .get("/albums/album/" + albumID)
      .then((response) => {
        setAlbumData(response.data);
      })
      .catch((error) => {
        setAlbumData([]);
        console.log(error);
      });

    return () => {
      setAlbumData(null);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (albumData === null) {
    return (
      <div className="grid place-items-center">
        <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />
      </div>
    );
  }
  return (
    <PageLayout className="p-8">
      <div className="overflow-hidden w-60 rounded-md">
        <img
          src={axios.defaults.baseURL + "/albums/cover/" + albumData.id}
          alt={albumData.name}
          width={250}
          height={250}
          className={"h-auto w-auto object-cover transition-all aspect-square"}
        />
      </div>
      <div className="mt-6 space-y-1">
        <h2 className="text-2xl font-semibold tracking-tight">
          {albumData.name}&apos;s songs
        </h2>
        <p className="text-sm text-muted-foreground">
          By {albumData.users.map((user) => user.name).join(", ")}
        </p>
      </div>
      <div className="relative my-4">
        <ScrollArea>
          <div className="flex space-x-4 pb-4">
            <Table>
              <TableCaption>
                A list of &ldquo;{albumData.name}&ldquo; album songs, put
                together by{" "}
                {albumData.users.map((user) => user.name).join(", ")}.
              </TableCaption>
              <TableHeader>
                <TableRow>
                  <TableHead className="w-[100px]"></TableHead>
                  <TableHead>Name</TableHead>
                  <TableHead>Artist(s)</TableHead>
                  <TableHead className="text-right">Total Listens</TableHead>
                </TableRow>
              </TableHeader>
              <TableBody>
                {albumData.medias.length === 0 && "No songs found"}
                {albumData.medias.map((media) => (
                  <TableRow
                    className="cursor-pointer"
                    onClick={() => {
                      setTrack(media);
                      setTracks(albumData.medias);
                      setPlayList(albumData);
                    }}
                    key={media.id}
                  >
                    <TableCell>
                      <Play className="h-4 w-4" />
                    </TableCell>
                    <TableCell className="font-medium">{media.name}</TableCell>
                    <TableCell>
                      {media.users.map((user) => user.name).join(", ")}
                    </TableCell>
                    <TableCell className="text-right">
                      {media.listensCount}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TableCell colSpan={3}>Total songs</TableCell>
                  <TableCell className="text-right">
                    {albumData.medias.length}
                  </TableCell>
                </TableRow>
              </TableFooter>
            </Table>
          </div>
          <ScrollBar orientation="horizontal" />
        </ScrollArea>
      </div>
    </PageLayout>
  );
}
