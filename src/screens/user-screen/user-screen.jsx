import { useContext, useEffect, useState } from "react";
import axios from "axios";

import { PageLayout, AlbumArtwork, MediaArtwork } from "@/components/layout";
import { Separator, ScrollArea, ScrollBar, Icons } from "@/components/ui";
import { AudioContext } from "@/contexts/audio-provider";
import { useNavigate, useParams } from "react-router-dom";

export function UserScreen() {
  const [mediaData, setMediaData] = useState(null);
  const [albumsData, setAlbumsData] = useState(null);
  const [usersData, setUsersData] = useState(null);

  const { setTrack, setTracks, setPlayList } = useContext(AudioContext);

  const { username } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get("/auth/search/" + username)
      .then((response) => {
        setUsersData(response.data);
      })
      .catch((error) => {
        setUsersData([]);
        console.log(error);
      });

    return () => {
      setUsersData(null);
    };
  }, [username]);

  useEffect(() => {
    if (usersData === null) return;

    axios
      .get("/albums/user/" + usersData[0].id)
      .then((response) => {
        setAlbumsData(response.data);
      })
      .catch((error) => {
        setUsersData([]);
        console.log(error);
      });

    axios
      .get("/media/user/" + usersData[0].id)
      .then((response) => {
        setMediaData(response.data);
      })
      .catch((error) => {
        setMediaData([]);
        console.log(error);
      });

    return () => {
      setAlbumsData(null);
    };
  }, [usersData]);

  if (usersData === null || mediaData === null || albumsData === null) {
    return (
      <div className="grid place-items-center">
        <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />
      </div>
    );
  }
  return (
    <PageLayout className="p-8">
      <div className="flex items-center justify-between">
        <div className="space-y-1">
          <h2 className="text-2xl font-semibold tracking-tight">
            {username[0].toUpperCase() + username.slice(1)}&apos;s songs
          </h2>
        </div>
      </div>
      <Separator className="my-4" />
      <div className="relative">
        <ScrollArea>
          <div className="flex space-x-4 pb-4">
            {mediaData.length === 0 && "No songs found"}
            {mediaData.map((media) => (
              <MediaArtwork
                onClick={() => {
                  setTrack(media);
                  setPlayList({});
                }}
                key={media.id}
                media={media}
                className="w-[150px]"
                aspectRatio="portrait"
                width={150}
                height={150}
              />
            ))}
          </div>
          <ScrollBar orientation="horizontal" />
        </ScrollArea>
      </div>
      <div className="mt-6 space-y-1">
        <h2 className="text-2xl font-semibold tracking-tight">
          {username[0].toUpperCase() + username.slice(1)}&apos;s Albums
        </h2>
      </div>
      <Separator className="my-4" />
      <div className="relative">
        <ScrollArea>
          <div className="flex space-x-4 pb-4">
            {albumsData.length === 0 && "No albums found"}
            {albumsData.map((album) => (
              <AlbumArtwork
                playClick={() => {
                  setTracks(album?.medias);
                  setPlayList(album);
                }}
                onClick={() => {
                  navigate(`/album/${album.id}`);
                }}
                key={album.id}
                album={album}
                className="w-[150px]"
                aspectRatio="square"
                width={150}
                height={150}
              />
            ))}
          </div>
          <ScrollBar orientation="horizontal" />
        </ScrollArea>
      </div>
    </PageLayout>
  );
}
