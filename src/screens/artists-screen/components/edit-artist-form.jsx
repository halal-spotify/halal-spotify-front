import { useContext, useEffect, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import {
  DialogFooter,
  DialogClose,
  Button,
  Label,
  Input,
  Icons,
  useToast,
} from "@/components/ui";
import { ArtistsContext } from "../artists-screen";

export function EditArtistForm({ artistID, setOpenDialog }) {
  const { toast } = useToast();
  const trackCtx = useContext(ArtistsContext);

  const [isLoading, setIsLoading] = useState(false);
  const [newArtistForm, setNewArtistForm] = useState(null);

  const handleChange = (event) => {
    setNewArtistForm({
      ...newArtistForm,
      [event.target.name]: event.target.value,
    });
  };

  function onSubmit(event) {
    event.preventDefault();
    setIsLoading(true);

    axios
      .put("/auth/users/" + artistID, newArtistForm)
      .then(function () {
        trackCtx.fetchData();
        toast({
          title: "Success! Updating artist",
          description: "Artist updated successfully.",
          variant: "success",
        });
        setOpenDialog(false);
        setIsLoading(false);
      })
      .catch(function (error) {
        console.log(error);
        toast({
          title: "Error! Updating artist",
          description:
            "Something bad happened, Artist was not updated, please try again.",
          variant: "destructive",
        });
        setIsLoading(false);
      });
  }

  function getMediaInfo() {
    const data = trackCtx.data.find((e) => e.id === artistID);
    setNewArtistForm({ ...data, password: "", ConfirmPassword: "" });
  }

  useEffect(() => {
    setIsLoading(true);
    if (newArtistForm === null) {
      getMediaInfo();
      setIsLoading(false);
    }

    return () => {
      setIsLoading(true);
      setNewArtistForm(null);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (newArtistForm === null)
    return (
      <div className="grid place-items-center">
        <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />
      </div>
    );

  const disable =
    newArtistForm.password.length !== 0
      ? newArtistForm.password !== newArtistForm.ConfirmPassword
      : false;

  return (
    <form onSubmit={onSubmit}>
      <div className="grid gap-4 py-4">
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="name" className="text-right">
            Name
          </Label>
          <Input
            id="name"
            name="name"
            placeholder="Ex: We're all in this together"
            className="col-span-3"
            disabled={isLoading}
            value={newArtistForm.name}
            onChange={handleChange}
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="email" className="text-right">
            Email
          </Label>
          <Input
            id="email"
            name="email"
            placeholder="21"
            type="email"
            className="col-span-3"
            disabled={isLoading}
            value={newArtistForm.email}
            onChange={handleChange}
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="password" className="text-right">
            Password (optional)
          </Label>
          <Input
            id="password"
            name="password"
            placeholder="***"
            type="password"
            className="col-span-3"
            disabled={isLoading}
            value={newArtistForm.password}
            onChange={handleChange}
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="ConfirmPassword" className="text-right">
            Confirm Password (optional)
          </Label>
          <Input
            id="ConfirmPassword"
            name="ConfirmPassword"
            placeholder="***"
            type="password"
            className="col-span-3"
            disabled={isLoading}
            value={newArtistForm.ConfirmPassword}
            onChange={handleChange}
          />
        </div>
      </div>
      <DialogFooter className="flex items-center justify-between space-x-2">
        <DialogClose asChild>
          <Button variant="ghost" type="reset">
            Cancel
          </Button>
        </DialogClose>
        <Button type="submit" disabled={isLoading || disable}>
          {isLoading && <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />}
          Save artist
        </Button>
      </DialogFooter>
    </form>
  );
}

EditArtistForm.propTypes = {
  artistID: PropTypes.number.isRequired,
  setOpenDialog: PropTypes.func.isRequired,
};
