import { createContext, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { PageLayout } from "@/components/layout";
import { useToast } from "@/components/ui";

import { columns } from "./components/columns";
import { DataTable } from "./components/data-table";

import axios from "axios";
import { useLocation } from "react-router-dom";

export const ArtistsContext = createContext();

export function ArtistsScreen({ menuOpen }) {
  const { state } = useLocation();

  const { toast } = useToast();
  const [data, setData] = useState([]);

  function fetchAllArtists() {
    axios
      .get("/auth/users")
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.log(error);
        toast({
          title: "Couldn't fetch data",
          description: "Please try again later.",
          variant: "warn",
        });
      });
  }

  function fetchOneArtist(id) {
    axios
      .get("/auth/users/" + id)
      .then((response) => {
        setData([response.data]);
      })
      .catch((error) => {
        console.log(error);
        toast({
          title: "Couldn't fetch data",
          description: "Please try again later.",
          variant: "warn",
        });
      });
  }

  useEffect(() => {
    if (state) return fetchOneArtist(state.id);
    fetchAllArtists();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ArtistsContext.Provider
      value={{ data, setData, fetchData: fetchAllArtists }}
    >
      <PageLayout className="p-8" menuOpen={menuOpen}>
        <div className="flex space-between items-center mb-4">
          <div>
            <h1 className="text-2xl font-semibold tracking-tight">Artists</h1>
            <p className="text-sm text-muted-foreground">List of all artists</p>
          </div>
        </div>

        <DataTable columns={columns} data={data} />
      </PageLayout>
    </ArtistsContext.Provider>
  );
}

ArtistsScreen.propTypes = {
  menuOpen: PropTypes.bool,
};
