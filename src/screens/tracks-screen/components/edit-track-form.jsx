import { useContext, useEffect, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import {
  DialogFooter,
  DialogClose,
  Button,
  Label,
  Input,
  Icons,
  useToast,
} from "@/components/ui";
import { FileDown } from "lucide-react";
import { TracksContext } from "../tracks-screen";

export function EditTrackForm({ mediaID, setOpenDialog }) {
  const { toast } = useToast();
  const trackCtx = useContext(TracksContext);

  const [isLoading, setIsLoading] = useState(false);
  const [newMediaForm, setNewMediaForm] = useState(null);

  const handleChange = (event) => {
    setNewMediaForm({
      ...newMediaForm,
      [event.target.name]: event.target.value,
    });
  };

  const handleMediaChange = (event) => {
    if (
      event.target.name === "imgFile" &&
      event.target.files[0].type !== "image/jpeg"
    ) {
      toast({
        title: "Bad media type",
        description: "Only `.jpg` format is accepted.",
        variant: "warn",
      });
    }

    if (
      event.target.name === "mediaFile" &&
      event.target.files[0].type !== "audio/mpeg"
    ) {
      toast({
        title: "Bad media type",
        description: "Only `.mp3` format is accepted.",
        variant: "warn",
      });
      console.log("bad media type");
    }

    setNewMediaForm({
      ...newMediaForm,
      [event.target.name]: event.target.files[0],
    });
  };

  function onSubmit(event) {
    event.preventDefault();
    setIsLoading(true);

    let newMedia = new FormData();

    //Adding files to the formdata
    newMedia.append(
      "mediaFile",
      newMediaForm.mediaFile ? newMediaForm.mediaFile : null,
    );
    newMedia.append(
      "imgFile",
      newMediaForm.imgFile ? newMediaForm.imgFile : null,
    );
    newMedia.append(
      "mediaName",
      newMediaForm.mediaName ? newMediaForm.mediaName : null,
    );
    newMedia.append("uid", newMediaForm.uid ? newMediaForm.uid : null);

    axios
      .put("/media/" + mediaID, newMediaForm, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then(function () {
        trackCtx.fetchData();
        toast({
          title: "Success! Updating media",
          description: "Media updated successfully.",
          variant: "success",
        });
        setOpenDialog(false);
        setIsLoading(false);
      })
      .catch(function (error) {
        console.log(error);
        toast({
          title: "Error! Updating media",
          description:
            "Something bad happened, media was not updated, please try again.",
          variant: "destructive",
        });
        setIsLoading(false);
      });
  }

  function getMediaInfo() {
    const data = trackCtx.data.find((e) => e.id === mediaID);
    setNewMediaForm({
      id: data.id,
      mediaName: data.name,
      uid: data.users.map((e) => e.id).join(", "),
      mediaFile: null,
      imgFile: null,
      updatedAt: data.updatedAt,
    });
  }

  useEffect(() => {
    setIsLoading(true);
    if (newMediaForm === null) {
      getMediaInfo();
      setIsLoading(false);
    }

    return () => {
      setIsLoading(true);
      setNewMediaForm(null);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (newMediaForm === null)
    return (
      <div className="grid place-items-center">
        <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />
      </div>
    );

  return (
    <form onSubmit={onSubmit}>
      <div className="grid gap-4 py-4">
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="mediaName" className="text-right">
            Name
          </Label>
          <Input
            id="mediaName"
            name="mediaName"
            placeholder="Ex: We're all in this together"
            className="col-span-3"
            disabled={isLoading}
            value={newMediaForm.mediaName}
            onChange={handleChange}
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="uid" className="text-right">
            User ID
          </Label>
          <Input
            id="uid"
            name="uid"
            placeholder="21"
            className="col-span-3"
            disabled={isLoading}
            value={newMediaForm.uid}
            onChange={handleChange}
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="imgFile" className="text-right">
            Cover (.jpg)
          </Label>
          <Input
            id="imgFile"
            name="imgFile"
            className="col-span-3"
            disabled={isLoading}
            onChange={handleMediaChange}
            type="file"
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="mediaFile" className="text-right">
            File (.mp3)
          </Label>
          <Input
            id="mediaFile"
            name="mediaFile"
            className="col-span-3"
            disabled={isLoading}
            onChange={handleMediaChange}
            type="file"
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="oldImgFile" className="text-right">
            File
          </Label>
          <a
            href={axios.defaults.baseURL + "/media/file/" + newMediaForm.id}
            download
            className="flex gap-2 underline col-span-3"
          >
            Download <FileDown />
          </a>
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="oldImgFile" className="text-right">
            Current Cover
          </Label>
          <img
            src={
              axios.defaults.baseURL +
              "/media/cover/" +
              newMediaForm.id +
              "?o=" +
              newMediaForm.updatedAt
            }
            alt="Current cover"
            className="col-span-3"
          />
        </div>
      </div>
      <DialogFooter className="flex items-center justify-between space-x-2">
        <DialogClose asChild>
          <Button variant="ghost" type="reset">
            Cancel
          </Button>
        </DialogClose>
        <Button type="submit" disabled={isLoading}>
          {isLoading && <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />}
          Save track
        </Button>
      </DialogFooter>
    </form>
  );
}

EditTrackForm.propTypes = {
  mediaID: PropTypes.number.isRequired,
  setOpenDialog: PropTypes.func.isRequired,
};
