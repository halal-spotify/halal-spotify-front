import { useContext, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import {
  DialogFooter,
  DialogClose,
  Button,
  Label,
  Input,
  Icons,
  useToast,
} from "@/components/ui";
import { TracksContext } from "../tracks-screen";

export function AddTrackForm({ setOpenDialog }) {
  const { toast } = useToast();
  const trackCtx = useContext(TracksContext);

  const [isLoading, setIsLoading] = useState(false);
  const [newMediaForm, setNewMediaForm] = useState({
    mediaFile: "",
    imgFile: "",
    mediaName: "",
    uid: "",
  });

  const handleChange = (event) => {
    setNewMediaForm({
      ...newMediaForm,
      [event.target.name]: event.target.value,
    });
  };

  const handleMediaChange = (event) => {
    if (
      event.target.name === "imgFile" &&
      event.target.files[0].type !== "image/jpeg"
    ) {
      toast({
        title: "Bad media type",
        description: "Only `.jpg` format is accepted.",
        variant: "warn",
      });
    }

    if (
      event.target.name === "mediaFile" &&
      event.target.files[0].type !== "audio/mpeg"
    ) {
      toast({
        title: "Bad media type",
        description: "Only `.mp3` format is accepted.",
        variant: "warn",
      });
      console.log("bad media type");
    }

    setNewMediaForm({
      ...newMediaForm,
      [event.target.name]: event.target.files[0],
    });
  };

  function onSubmit(event) {
    event.preventDefault();
    setIsLoading(true);

    let newMedia = new FormData();

    //Adding files to the formdata
    newMedia.append("mediaFile", newMediaForm.mediaFile);
    newMedia.append("imgFile", newMediaForm.imgFile);
    newMedia.append("mediaName", newMediaForm.mediaName);
    newMedia.append("uid", newMediaForm.uid);

    axios
      .post("/media/upload", newMediaForm, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then(function () {
        trackCtx.fetchData();
        toast({
          title: "Success! Add new media",
          description: "Media added successfully.",
          variant: "success",
        });
        setOpenDialog(false);
        setIsLoading(false);
      })
      .catch(function (error) {
        console.log(error);
        toast({
          title: "Error! Add new media",
          description:
            "Something bad happened, media was not added, please try again.",
          variant: "destructive",
        });
        setIsLoading(false);
      });
  }

  const disableUpload =
    newMediaForm.mediaName === "" ||
    newMediaForm.uid === "" ||
    newMediaForm.mediaFile === "" ||
    newMediaForm.imgFile === "";

  return (
    <form onSubmit={onSubmit}>
      <div className="grid gap-4 py-4">
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="mediaName" className="text-right">
            Name
          </Label>
          <Input
            id="mediaName"
            name="mediaName"
            placeholder="Ex: We're all in this together"
            className="col-span-3"
            disabled={isLoading}
            value={newMediaForm.mediaName}
            onChange={handleChange}
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="uid" className="text-right">
            User ID
          </Label>
          <Input
            id="uid"
            name="uid"
            placeholder="21"
            className="col-span-3"
            disabled={isLoading}
            value={newMediaForm.uid}
            onChange={handleChange}
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="imgFile" className="text-right">
            Cover (.jpg)
          </Label>
          <Input
            id="imgFile"
            name="imgFile"
            className="col-span-3"
            disabled={isLoading}
            onChange={handleMediaChange}
            type="file"
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="mediaFile" className="text-right">
            File (.mp3)
          </Label>
          <Input
            id="mediaFile"
            name="mediaFile"
            className="col-span-3"
            disabled={isLoading}
            onChange={handleMediaChange}
            type="file"
          />
        </div>
      </div>
      <DialogFooter className="flex items-center justify-between space-x-2">
        <DialogClose asChild>
          <Button variant="ghost" type="reset">
            Cancel
          </Button>
        </DialogClose>
        <Button type="submit" disabled={isLoading || disableUpload}>
          {isLoading && <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />}
          Save track
        </Button>
      </DialogFooter>
    </form>
  );
}

AddTrackForm.propTypes = {
  setOpenDialog: PropTypes.func.isRequired,
};
