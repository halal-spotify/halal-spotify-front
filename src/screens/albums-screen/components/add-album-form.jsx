import { useContext, useEffect, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import {
  DialogFooter,
  DialogClose,
  Button,
  Label,
  Input,
  Icons,
  useToast,
  Select,
  SelectTrigger,
  SelectValue,
  SelectContent,
  SelectGroup,
  SelectLabel,
  SelectItem,
  Table,
  TableHeader,
  TableRow,
  TableHead,
  Checkbox,
  TableBody,
  TableCell,
} from "@/components/ui";
import { AlbumsContext } from "../albums-screen";

export function AddAlbumForm({ setOpenDialog }) {
  const albumsCtx = useContext(AlbumsContext);
  const { toast } = useToast();

  const [isLoading, setIsLoading] = useState(false);
  const [newAlbumForm, setNewAlbumForm] = useState({
    imgFile: "",
    albumName: "",
    uid: "",
  });
  const [selectedSongs, setSelectedSongs] = useState([]);

  const [artists, setArtists] = useState([]);
  const [songs, setSongs] = useState([]);

  const handleChange = (event) => {
    setNewAlbumForm({
      ...newAlbumForm,
      [event.target.name]: event.target.value,
    });
  };

  const handleAlbumChange = (event) => {
    if (
      event.target.name === "imgFile" &&
      event.target.files[0].type !== "image/jpeg"
    ) {
      toast({
        title: "Bad album type",
        description: "Only `.jpg` format is accepted.",
        variant: "warn",
      });
    }

    if (
      event.target.name === "AlbumFile" &&
      event.target.files[0].type !== "audio/mpeg"
    ) {
      toast({
        title: "Bad album type",
        description: "Only `.mp3` format is accepted.",
        variant: "warn",
      });
      console.log("bad album type");
    }

    setNewAlbumForm({
      ...newAlbumForm,
      [event.target.name]: event.target.files[0],
    });
  };

  function onSubmit(event) {
    event.preventDefault();
    setIsLoading(true);

    let newAlbum = new FormData();

    //Adding files to the formdata
    newAlbum.append("coverPath", newAlbumForm.imgFile);
    newAlbum.append("name", newAlbumForm.albumName);
    newAlbum.append("userID", newAlbumForm.uid);
    newAlbum.append("mediaIDs", selectedSongs);

    axios
      .post("/albums", newAlbum, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then(function () {
        albumsCtx.fetchData();
        toast({
          title: "Success! Add new media",
          description: "Album added successfully.",
          variant: "success",
        });
        setOpenDialog(false);
        setIsLoading(false);
      })
      .catch(function (error) {
        console.log(error);
        toast({
          title: "Error! Add new media",
          description:
            "Something bad happened, media was not added, please try again.",
          variant: "destructive",
        });
        setIsLoading(false);
      });
  }

  function fetchArtists() {
    axios
      .get("/auth/users")
      .then(function (response) {
        console.log(response);
        setArtists(response.data);
      })
      .catch(function (error) {
        toast({
          title: "Error! Fetch artists",
          description:
            "Something bad happened, artists were not fetched, please try again.",
          variant: "destructive",
        });
        console.log(error);
      });
  }

  function fetchSongs() {
    axios
      .get("/media")
      .then(function (response) {
        console.log(response);
        setSongs(response.data);
      })
      .catch(function (error) {
        toast({
          title: "Error! Fetch songs",
          description:
            "Something bad happened, songs were not fetched, please try again.",
          variant: "destructive",
        });
        console.log(error);
      });
  }

  useEffect(() => {
    fetchArtists();
    fetchSongs();
    return () => {
      setArtists([]);
      setSongs([]);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const disableUpload =
    newAlbumForm.albumName === "" ||
    newAlbumForm.uid === "" ||
    newAlbumForm.imgFile === "" ||
    selectedSongs.length === 0;

  return (
    <form onSubmit={onSubmit}>
      <div className="grid gap-4 py-4">
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="albumName" className="text-right">
            Name
          </Label>
          <Input
            id="albumName"
            name="albumName"
            placeholder="Ex: We're all in this together"
            className="col-span-3"
            disabled={isLoading}
            value={newAlbumForm.albumName}
            onChange={handleChange}
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="uid" className="text-right">
            Artist
          </Label>
          <Select
            id="uid"
            name="uid"
            onValueChange={(value) => {
              handleChange({
                target: { name: "uid", value: value },
              });
            }}
            defaultValue={newAlbumForm.uid}
          >
            <SelectTrigger className="col-span-3">
              <SelectValue placeholder="Select an artist..." />
            </SelectTrigger>
            <SelectContent className="col-span-3">
              <SelectGroup>
                <SelectLabel>Artist list</SelectLabel>
                {artists.map((artist) => (
                  <SelectItem key={artist.id} value={artist.id.toString()}>
                    {artist.name}
                  </SelectItem>
                ))}
              </SelectGroup>
            </SelectContent>
          </Select>
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="imgFile" className="text-right">
            Cover (.jpg)
          </Label>
          <Input
            id="imgFile"
            name="imgFile"
            className="col-span-3"
            disabled={isLoading}
            onChange={handleAlbumChange}
            type="file"
          />
        </div>
        <div className="items-center gap-4">
          <Label htmlFor="albumFile" className="text-right">
            Select one or multiple songs
          </Label>
          <div className="border rounded-lg overflow-hidden mt-2">
            <Table>
              <TableHeader>
                <TableRow>
                  <TableHead className="w-[32px]">
                    <Checkbox
                      checked={selectedSongs.length === songs.length}
                      onCheckedChange={() => {
                        if (selectedSongs.length === songs.length) {
                          setSelectedSongs([]);
                        } else {
                          setSelectedSongs(songs.map((song) => song.id));
                        }
                      }}
                      id="select-all"
                    />
                  </TableHead>
                  <TableHead>Title</TableHead>
                  <TableHead>Author</TableHead>
                </TableRow>
              </TableHeader>
              <TableBody>
                {songs.map((song) => (
                  <TableRow key={song.id}>
                    <TableCell>
                      <Checkbox
                        checked={selectedSongs.includes(song.id)}
                        onCheckedChange={(value) =>
                          setSelectedSongs((prev) => {
                            if (value) {
                              return [...prev, song.id];
                            } else {
                              return prev.filter((id) => id !== song.id);
                            }
                          })
                        }
                        id={song.id}
                      />
                    </TableCell>
                    <TableCell className="font-medium">{song.name}</TableCell>
                    <TableCell>
                      {song.users.map((user) => user.name).join(", ")}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </div>
        </div>
      </div>
      <DialogFooter className="flex items-center justify-between space-x-2">
        <DialogClose asChild>
          <Button variant="ghost" type="reset">
            Cancel
          </Button>
        </DialogClose>
        <Button type="submit" disabled={isLoading || disableUpload}>
          {isLoading && <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />}
          Save album
        </Button>
      </DialogFooter>
    </form>
  );
}

AddAlbumForm.propTypes = {
  setOpenDialog: PropTypes.func,
};
