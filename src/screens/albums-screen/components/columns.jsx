import { DataTableColumnHeader } from "./data-table-column-header";

export const columns = [
  {
    accessorKey: "id",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="ID" />
    ),
    cell: ({ row }) => <div className="w-[80px]">{row.getValue("id")}</div>,
    enableSorting: false,
    enableHiding: false,
  },
  {
    accessorKey: "name",
    title: "Title",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="Title" />
    ),
    cell: ({ row }) => {
      return (
        <div className="flex space-x-2">
          <span className="max-w-[500px] truncate font-medium">
            {row.getValue("name")}
          </span>
        </div>
      );
    },
  },
  {
    accessorKey: "users",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="Artist" />
    ),
    title: "Artist(s)",
    cell: ({ row }) => {
      return (
        <div className="flex space-x-2">
          <span className="max-w-[500px] truncate font-medium">
            {row
              .getValue("users")
              .map((user) => user.name)
              .join(", ")}
          </span>
        </div>
      );
    },
  },
  {
    accessorKey: "medias",
    title: "N°# Tracks",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="N°# Tracks" />
    ),
    cell: ({ row }) => {
      return (
        <div className="flex space-x-2">
          <span className="max-w-[500px] truncate font-medium">
            {row.getValue("medias").length}
          </span>
        </div>
      );
    },
  },
  {
    accessorKey: "createdAt",
    title: "Created At",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="Created At" />
    ),
    cell: ({ row }) => {
      const date = new Date(row.getValue("createdAt"));
      return (
        <div className="flex space-x-2">
          <span className="max-w-[500px] truncate font-medium">
            {date.toLocaleString()}
          </span>
        </div>
      );
    },
  },
];
