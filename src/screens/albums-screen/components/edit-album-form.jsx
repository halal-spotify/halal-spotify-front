import { useContext, useEffect, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import {
  DialogFooter,
  DialogClose,
  Button,
  Label,
  Input,
  Icons,
  useToast,
  Select,
  SelectTrigger,
  SelectValue,
  SelectContent,
  SelectGroup,
  SelectLabel,
  SelectItem,
  Table,
  TableHeader,
  TableRow,
  TableHead,
  Checkbox,
  TableBody,
  TableCell,
} from "@/components/ui";
import { AlbumsContext } from "../albums-screen";

export function EditAlbumForm({ albumID, setOpenDialog }) {
  const { toast } = useToast();
  const albumsCtx = useContext(AlbumsContext);

  const [isLoading, setIsLoading] = useState(false);
  const [AlbumForm, setAlbumForm] = useState(null);
  const [selectedSongs, setSelectedSongs] = useState([]);

  const [artists, setArtists] = useState([]);
  const [songs, setSongs] = useState([]);

  const handleChange = (event) => {
    setAlbumForm({
      ...AlbumForm,
      [event.target.name]: event.target.value,
    });
  };

  const handleAlbumChange = (event) => {
    if (
      event.target.name === "imgFile" &&
      event.target.files[0].type !== "image/jpeg"
    ) {
      toast({
        title: "Bad album type",
        description: "Only `.jpg` format is accepted.",
        variant: "warn",
      });
    }

    if (
      event.target.name === "AlbumFile" &&
      event.target.files[0].type !== "audio/mpeg"
    ) {
      toast({
        title: "Bad album type",
        description: "Only `.mp3` format is accepted.",
        variant: "warn",
      });
      console.log("bad album type");
    }

    setAlbumForm({
      ...AlbumForm,
      [event.target.name]: event.target.files[0],
    });
  };

  function onSubmit(event) {
    event.preventDefault();
    setIsLoading(true);

    let newAlbum = new FormData();

    //Adding files to the formdata
    newAlbum.append("id", albumID);
    newAlbum.append("coverPath", AlbumForm.imgFile);
    newAlbum.append("name", AlbumForm.albumName);
    newAlbum.append("userID", AlbumForm.uid);
    for (let i = 0; i < selectedSongs.length; i++) {
      newAlbum.append("mediaIDs", selectedSongs[i]);
    }

    axios
      .put("/albums", newAlbum, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then(function () {
        albumsCtx.fetchData();
        toast({
          title: "Success! update new media",
          description: "Album updated successfully.",
          variant: "success",
        });
        setOpenDialog(false);
        setIsLoading(false);
      })
      .catch(function (error) {
        console.log(error);
        toast({
          title: "Error! update new media",
          description:
            "Something bad happened, media was not updated, please try again.",
          variant: "destructive",
        });
        setIsLoading(false);
      });
  }

  function fetchArtists() {
    axios
      .get("/auth/users")
      .then(function (response) {
        console.log(response);
        setArtists(response.data);
      })
      .catch(function (error) {
        toast({
          title: "Error! Fetch artists",
          description:
            "Something bad happened, artists were not fetched, please try again.",
          variant: "destructive",
        });
        console.log(error);
      });
  }

  function fetchSongs() {
    axios
      .get("/media")
      .then(function (response) {
        console.log(response);
        setSongs(response.data);
      })
      .catch(function (error) {
        toast({
          title: "Error! Fetch songs",
          description:
            "Something bad happened, songs were not fetched, please try again.",
          variant: "destructive",
        });
        console.log(error);
      });
  }

  function fetchAlbumDetails() {
    const data = albumsCtx.data.find((e) => e.id === albumID);
    setAlbumForm({
      ...data,
      imgFile: null,
      albumName: data.name,
      uid: data.users[0]?.id.toString(),
      updatedAt: data.updatedAt,
    });
    setSelectedSongs(data.medias.map((media) => media.id));
  }

  useEffect(() => {
    if (AlbumForm === null && songs.length === 0 && artists.length === 0) {
      fetchAlbumDetails();
      fetchArtists();
      fetchSongs();
      setIsLoading(false);
    }
    return () => {
      setArtists([]);
      setSongs([]);
      setAlbumForm(null);
      setSelectedSongs([]);
      setIsLoading(true);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (AlbumForm === null)
    return (
      <div className="grid place-items-center">
        <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />
      </div>
    );

  return (
    <form onSubmit={onSubmit}>
      <div className="grid gap-4 py-4">
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="albumName" className="text-right">
            Name
          </Label>
          <Input
            id="albumName"
            name="albumName"
            placeholder="Ex: We're all in this together"
            className="col-span-3"
            disabled={isLoading}
            value={AlbumForm.albumName}
            onChange={handleChange}
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="uid" className="text-right">
            Artist
          </Label>
          <Select
            id="uid"
            name="uid"
            onValueChange={(value) => {
              handleChange({
                target: { name: "uid", value: value },
              });
            }}
            defaultValue={AlbumForm.uid}
          >
            <SelectTrigger className="col-span-3">
              <SelectValue placeholder="Select an artist..." />
            </SelectTrigger>
            <SelectContent className="col-span-3">
              <SelectGroup>
                <SelectLabel>Artist list</SelectLabel>
                {artists.map((artist) => (
                  <SelectItem key={artist.id} value={artist.id.toString()}>
                    {artist.name}
                  </SelectItem>
                ))}
              </SelectGroup>
            </SelectContent>
          </Select>
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="imgFile" className="text-right">
            Cover (.jpg)
          </Label>
          <Input
            id="imgFile"
            name="imgFile"
            className="col-span-3"
            disabled={isLoading}
            onChange={handleAlbumChange}
            type="file"
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="oldImgFile" className="text-right">
            Current Cover
          </Label>
          <img
            src={
              axios.defaults.baseURL +
              "/albums/cover/" +
              albumID +
              "?o=" +
              AlbumForm.updatedAt
            }
            alt="Current cover"
            className="col-span-3"
          />
        </div>
        <div className="items-center gap-4">
          <Label htmlFor="albumFile" className="text-right">
            Select one or multiple songs
          </Label>
          <div className="border rounded-lg overflow-hidden mt-2">
            <Table>
              <TableHeader>
                <TableRow>
                  <TableHead className="w-[32px]">
                    <Checkbox
                      checked={selectedSongs.length === songs.length}
                      onCheckedChange={() => {
                        if (selectedSongs.length === songs.length) {
                          setSelectedSongs([]);
                        } else {
                          setSelectedSongs(songs.map((song) => song.id));
                        }
                      }}
                      id="select-all"
                    />
                  </TableHead>
                  <TableHead>Title</TableHead>
                  <TableHead>Author</TableHead>
                </TableRow>
              </TableHeader>
              <TableBody>
                {songs.map((song) => (
                  <TableRow key={song.id}>
                    <TableCell>
                      <Checkbox
                        checked={selectedSongs.includes(song.id)}
                        onCheckedChange={(value) =>
                          setSelectedSongs((prev) => {
                            if (value) {
                              return [...prev, song.id];
                            } else {
                              return prev.filter((id) => id !== song.id);
                            }
                          })
                        }
                        id={song.id}
                      />
                    </TableCell>
                    <TableCell className="font-medium">{song.name}</TableCell>
                    <TableCell>
                      {song.users.map((user) => user.name).join(", ")}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </div>
        </div>
      </div>
      <DialogFooter className="flex items-center justify-between space-x-2">
        <DialogClose asChild>
          <Button variant="ghost" type="reset">
            Cancel
          </Button>
        </DialogClose>
        <Button type="submit" disabled={isLoading}>
          {isLoading && <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />}
          Save album
        </Button>
      </DialogFooter>
    </form>
  );
}

EditAlbumForm.propTypes = {
  albumID: PropTypes.number,
  setOpenDialog: PropTypes.func,
};
