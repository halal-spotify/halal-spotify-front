import PropTypes from "prop-types";
import { PageLayout } from "@/components/layout";
import { useToast } from "@/components/ui";

import { columns } from "./components/columns";
import { DataTable } from "./components/data-table";

import { createContext, useEffect, useState } from "react";
import axios from "axios";
import { useLocation } from "react-router-dom";

export const AlbumsContext = createContext();

export function AlbumsScreen({ menuOpen }) {
  const { state } = useLocation();

  const { toast } = useToast();
  const [data, setData] = useState([]);

  function fetchAllAlbums() {
    axios
      .get("/albums")
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.log(error);
        toast({
          title: "Couldn't fetch data",
          description: "Please try again later.",
          variant: "warn",
        });
      });
  }

  const fetchOneAlbum = (id) => {
    axios
      .get("/albums/album/" + id)
      .then((response) => {
        setData([response.data]);
      })
      .catch((error) => {
        console.log(error);
        toast({
          title: "Couldn't fetch data",
          description: "Please try again later.",
          variant: "warn",
        });
      });
  };

  useEffect(() => {
    if (state) {
      return fetchOneAlbum(state.id);
    }
    fetchAllAlbums();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <AlbumsContext.Provider value={{ data, setData, fetchAllAlbums }}>
      <PageLayout className="p-8" menuOpen={menuOpen}>
        <div className="flex space-between items-center mb-4">
          <div>
            <h1 className="text-2xl font-semibold tracking-tight">Albums</h1>
            <p className="text-sm text-muted-foreground">List of all Albums</p>
          </div>
        </div>

        <DataTable columns={columns} data={data} />
      </PageLayout>
    </AlbumsContext.Provider>
  );
}

AlbumsScreen.propTypes = {
  menuOpen: PropTypes.bool,
};
